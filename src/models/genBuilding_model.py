from flask import current_app
from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
from flask import jsonify
import json
from .convertData import dictDecimalToInt

db = SQLAlchemy(current_app)

class CREATE_QR(db.Model):
    qrCodeID = db.Column(db.Integer, primary_key=True)
    userName = db.Column(db.String(100), nullable=False)
    code = db.Column(db.String(100), nullable=False)
    Building = db.Column(db.String(100), nullable=False)
    event1 = db.Column(db.String(100), nullable=False)
    RECORD_STATUS = db.Column(db.String(20), nullable=False, default='N')
    CREATE_DATE = db.Column(db.DateTime, nullable=False,
                            default=datetime.utcnow)
    LAST_DATE = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)

    def __repr__(self):
        return 'Blog post ' + str(self.qrCodeID)


def select_by_filter(filter=None):
    sql = " SELECT * "
    sql += " FROM CREATE_QR "
    sql += "WHERE RECORD_STATUS IN ('N')"
    print(filter)
    if filter:
        for k, v in filter.items():
            sql += " AND {0} = '{1}' ".format(k.upper(), v)
    print(sql)
    result = db.engine.execute(sql)
    result_data = [dictDecimalToInt(dict(r)) for r in result]
    if(len(result_data) > 0):
        return result_data
    else:
        return None
    # result_json = json.dumps()
    # return result_json
