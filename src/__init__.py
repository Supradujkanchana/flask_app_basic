from flask import Flask, render_template, Blueprint
from flask_limiter import Limiter 
# Flask,render_template เริ่มต้นใช้ Flask Main
# Limiter สำหรับ กำหนด limit Call
# Swagger UI สำหรับสร้าง Document


def create_app():
    # สร้าง app (core server) ภายในไฟล์
    app = Flask(__name__)

    # Config app
    app.config.from_object('src.config')

    # limiter = Limiter(
    #     app,
    #     key_func=get_remote_address,

    #     # set default limit
    #     # example set
    #     # default_limits=["2 per minute", "1 per second"]
    #     # default_limits=["1000 per day", "1 per second"],
    #     default_limits=["3000 per day"],
    # )

   # Binds the application only
    with app.app_context():

        # import file bluprint ex. emp.py
        from .controllers import genBuilding
        from .controllers.restplus import api

        blueprint = Blueprint('api', __name__, url_prefix='/api')
        api.init_app(blueprint)
        app.register_blueprint(blueprint)

        # emp.emp_bp from emp.py
        # app.register_blueprint(emp.emp_bp, url_prefix="/api/v2/emp")

        # set route index path
        @app.route('/')
        def index():
            return render_template('index.html')

    return app
