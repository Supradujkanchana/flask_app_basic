import urllib

DEBUG = True # Turns on debugging features in Flask สำหรับเปิด debug
JSON_SORT_KEYS = False #การเรียง JSON
SQLALCHEMY_TRACK_MODIFICATIONS = False # ปิด Warning

# Localhost
params = urllib.parse.quote_plus("DRIVER={SQL Server};Server=10.12.112.199;Database=TU_API;UID=thep_p;PWD=J@y5ckth;") 
# params = urllib.parse.quote_plus("DRIVER={SQL Server};Server=localhost;Database=TU_API;UID=sa;PWD=P@55w0rd;") 

# Windows Server
# params = urllib.parse.quote_plus("DRIVER={SQL Server};Server=10.12.112.199;Database=TU_API;UID=sup_pra;PWD=SQL$3rv3r;")

# Docker Server 
# params = urllib.parse.quote_plus("DRIVER={FreeTDS};Server=10.12.112.199;PORT=1433;Database=TU_API;UID=sup_pra;PWD=SQL$3rv3r;TDS_Version=8.0;")

# MAC
# params = urllib.parse.quote_plus("DRIVER={FreeTDS};Server=10.12.112.199;PORT=1433;Database=TU_API;UID=sup_pra;PWD=SQL$3rv3r;TDS_Version=8.0;")
# 
SQLALCHEMY_DATABASE_URI = "mssql+pyodbc:///?odbc_connect=%s" % params
# SQLALCHEMY_DATABASE_URI = 'sqlite:///posts.db'