from flask_restplus import Api

api = Api(version='1.0', title='My Building API',
            description='A simple demonstration of a Flask Rest Plus powered API')