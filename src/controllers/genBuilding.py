from flask import current_app, render_template, request, redirect, jsonify
from flask_restplus import fields, Resource
from .restplus import api
# from ..models.genBuilding_model import genBuilding_model.CREATE_QR, db
from ..models import genBuilding_model

dbQR = genBuilding_model.db

building_qr = api.model('Building QR', {
    'qrCodeID': fields.Integer(description='ID QR Code'),
    'userName': fields.String(description='UserName title'),
    'Building': fields.String(description='Building content')
})

building_code = api.model('Building Code', {
    'code': fields.String(description='code QR'),
    'userName': fields.String(description='UserName title'),
    'Building': fields.String(description='Building content')
})


building_qr_resp = api.model('Building QR Restp', {
    'status': fields.Boolean(description='resp post status'),
    'Data': fields.Nested(building_code, description='data from post')
})

ns = api.namespace('genBuilding', description='Building categories')

@ns.route('/getData')
class BuildingCollection(Resource):
    @api.marshal_list_with(building_qr)
    def get(self):
        Building = genBuilding_model.select_by_filter()
        return Building

    @api.expect(building_code)
    @api.marshal_list_with(building_qr_resp)
    def post(self):
        bodyObj = request.json
        print(bodyObj)
        new_post = genBuilding_model.CREATE_QR(
            userName=bodyObj.get('userName'),
            code=bodyObj.get('code'),
            Building=bodyObj.get('Building'))
        print(new_post)
        dbQR.session.add(new_post)
        dbQR.session.commit()
        print(new_post)
        response = {
            'status': True,
            'Data': bodyObj
        }
        return response, 201

@current_app.route('/genBuilding', methods=['GET', 'POST'])
def genBuilding():
    if request.method == 'POST':
        bodyObj = request.get_json(silent=True)
        new_post = genBuilding_model.CREATE_QR(
            userName=bodyObj.get('userName'),
            code=bodyObj.get('code'),
            Building=bodyObj.get('Building'))
        dbQR.session.add(new_post)
        dbQR.session.commit()
        response = {
            'status': True,
            'Data': bodyObj
        }
        return jsonify(response), 200
        # return '200'
    else:
        filter = request.args.to_dict()
        print('Controller', filter)
        return jsonify(genBuilding_model.select_by_filter(filter))


# @current_app.route('/genBuilding/delete/<int:qrCodeID>')
# def delete(qrCodeID):
#     post = genBuilding_model.CREATE_QR.query.get_or_404(qrCodeID)
#     dbQR.session.delete(post)
#     dbQR.session.commit()
#     return redirect('/genBuilding')


@current_app.route('/genBuilding/delete/<int:qrCodeID>', methods=['POST'])
def delete(qrCodeID):
    post = genBuilding_model.CREATE_QR.query.get_or_404(qrCodeID)
    if request.method == 'POST':
        post.RECORD_STATUS = 'D'
        dbQR.session.commit()
        response = {
            'status': True
        }
        return jsonify(response), 200


@current_app.route('/genBuilding/edit/<int:qrCodeID>', methods=['POST'])
def edit(qrCodeID):
    post = genBuilding_model.CREATE_QR.query.get_or_404(qrCodeID)
    print('post', post)
    bodyObj = request.get_json(silent=True)
    print('bodyObj', bodyObj)

    if request.method == 'POST':
        post.userName = bodyObj.get('userName')
        post.code = bodyObj.get('code')
        post.Building = bodyObj.get('Building')
        dbQR.session.commit()
        response = {
            'status': True,
            'Data': bodyObj
        }
        return jsonify(response), 200
