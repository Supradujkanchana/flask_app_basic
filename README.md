# flask Basic

## Setup
### step 0 Install Python

### step 1 Python install virtual environments packages (for create environments)
pip install virtualenvwrapper-win

### step 2 Python create virtual environments
mkvirtualenv flask_server

### step 3 Python work on virtual environments
workon flask_server

### step 4 Python install packages in virtual environments
* pip install -r requirements.txt

> if have list packages in requirements.txt file
* pip install _{Name packages}_


## Start project flask
### step 1 Python work on virtual environments
workon _{Name virtual environments}_

### step 2 Run Python
* python app.py
* flask run
> if SET environments flask




### MAC SET First System
pip3 install virtualenv
virtualenv flask_server
source flask_server/bin/activate
pip3 install -r requirements.txt
python3 app.py

### MAC Start project 
source flask_server/bin/activate
python3 app.py
### ADD LIB
brew install unixodbc
brew install freetds

### DOCKER build and complie 
docker build -t flask_api:latest .
docker run --name flask -i -d -p 5000:5001 flask_api:latest
docker logs 0be334142200